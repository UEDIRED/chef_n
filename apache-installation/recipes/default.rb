# directory '/etc/vidu' do
#     owner 'root'
#     group 'root'
#     mode '0766'
#     action :create
# end


#
# Cookbook:: apache
# Recipe:: default
#
# Copyright:: 2022, Navoda Vidushani

# Install apache2
package "apache2" do  
    action :install
end

service "apache2" do  
    action [:enable, :start]
end

cookbook_file "/var/www/index.html" do  
    source "index.html"  
    # The file's owner can read and write (6) 
    # Users in the same group as the file's owner can read (first 4) 
    # All users can read (second 4)
    mode "0644"
end
